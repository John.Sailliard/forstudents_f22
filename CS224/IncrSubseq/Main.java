import java.util.function.BiFunction;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
  public static void main(String argv[]) {
    testOne();
//  testTwo();
  }

  public static void testOne() {
    // define a Bifunction that will take two Objects that are each
    // a String and return true if all of the letters of the first
    // string appear in the second string

    List<Object> list = new ArrayList<Object>();
    list.add("at");
    list.add("tar");
    list.add("rot");
    list.add("rate");
    list.add("rest");
    list.add("irate");
    list.add("tired");
    list.add("attired");
    list.add("triangle");

    System.out.print("[ ");
    for (Object o: list)
      System.out.print((String) o + " ");
    System.out.println("]");

    Analyzer analyzer = new Analyzer(compFunction);
    int result = analyzer.analyze(list);
    System.out.println("result is " + result);
  } // testOne()

  public static void testTwo() {
    // define a BiFunction that will take two Objects that are each an int
    // and return true if the second one is greater than the first
    List<Object> list = Arrays.asList(4, 10, 5, 6, 11, 7, 13, 2, 15);

    for (Object o: list)
      System.out.print((int) o + " ");
    System.out.println();

    Analyzer analyzer = new Analyzer(compFunction);
    int result = analyzer.analyze(list);
    System.out.println("result is " + result);
  } // testTwo()
}
