// CS224 Fall 2022

public class Edge {
  Node n1;
  Node n2;
  boolean hasFlow;

  public Edge(Node n1, Node n2) {
    this.n1 = n1;
    this.n2 = n2;
    hasFlow = false;
  }

  @Override
  public String toString() {
    String s = this.n1 + " -> " + this.n2;
    if (this.hasFlow)
      s = s + " (1)";
    else
      s = s + " (0)";
    return s;
  }
}
