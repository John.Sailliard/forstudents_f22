// CS224 Fall 2022

import java.util.ArrayList;
import java.util.List;

public class Graph {
  List<Node> nodes;

  public Graph() {
    nodes = new ArrayList<Node> ();
  }

  //=========================================================

  public void addNode(int i, Object o) {
    Node node = new Node(i, o);
    nodes.add(node);
  }

  //=========================================================

  public void addEdge(int idx, Object o2) {
    Node nodeOne = null;
    Node nodeTwo = null;

    for (Node node: this.nodes) {
      if (node.name == idx) {
        nodeOne = node;
        break;
      }
    }
  
    for (Node node: this.nodes) {
      if (node.o == o2) {
        nodeTwo = node;
        break;
      }
    }

    if (nodeOne != null && nodeTwo != null) {
      Edge edge = new Edge(nodeOne, nodeTwo);
      nodeOne.addEdge(edge);
    }
  }

  //=========================================================

  public void addEdge(Object o1, int idx) {
    Node nodeOne = null;
    Node nodeTwo = null;

    for (Node node: this.nodes) {
      if (node.o == o1) {
        nodeOne = node;
        break;
      }
    }

    for (Node node: this.nodes) {
      if (node.name == idx) {
        nodeTwo = node;
        break;
      }
    }
  
    if (nodeOne != null && nodeTwo != null) {
      Edge edge = new Edge(nodeOne, nodeTwo);
      nodeOne.addEdge(edge);
    }
  }

  //=========================================================

  public void addEdge(Object o1, Object o2) {
    Node nodeOne = null;
    Node nodeTwo = null;

    for (Node node: this.nodes) {
      if (node.o == o1) {
        nodeOne = node;
        break;
      }
    }

    for (Node node: this.nodes) {
      if (node.o == o2) {
        nodeTwo = node;
        break;
      }
    }

    if (nodeOne != null && nodeTwo != null) {
      Edge edge = new Edge(nodeOne, nodeTwo);
      nodeOne.addEdge(edge);
    }
  } // addEdge()

  //=========================================================

  public boolean checkFlow(int sourceName, int sinkName) {
    // check that flow out of s == flow into t

    // implement this
  } // checkFlow()

  //=========================================================

  public boolean bruteForceSolve(int sourceName, int sinkName) {

    // implement this

  } // bruteForceSolve();

  //===================================================

  public void print() {
    for (Node n1: this.nodes) {
      for (Edge e: n1.adjlist) {
        System.out.println(e);
      }
    }
  }

} // class Graph
