// CS224 Fall 2022

public class Position {
  String name;

  public Position(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return this.name;
  }
}
